object Form1: TForm1
  Left = 247
  Top = 169
  BorderStyle = bsNone
  Caption = 'Form1'
  ClientHeight = 533
  ClientWidth = 489
  Color = clSilver
  TransparentColorValue = clGreen
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LbPontosTuristicos: TLabel
    Left = 160
    Top = 16
    Width = 194
    Height = 25
    Caption = 'Pontos Turisticos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbNome: TLabel
    Left = 232
    Top = 280
    Width = 47
    Height = 19
    Caption = 'Nome'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbLocal: TLabel
    Left = 232
    Top = 328
    Width = 47
    Height = 19
    Caption = 'Local'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbValor: TLabel
    Left = 232
    Top = 376
    Width = 46
    Height = 19
    Caption = 'Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbAtualizar: TLabel
    Left = 216
    Top = 424
    Width = 79
    Height = 19
    Caption = 'Atualizar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object ListBoxPontos: TListBox
    Left = 144
    Top = 56
    Width = 225
    Height = 201
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ItemHeight = 25
    ParentFont = False
    TabOrder = 0
  end
  object BtnInserir: TButton
    Left = 56
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Inserir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BtnInserirClick
  end
  object BtnDeletar: TButton
    Left = 56
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Deletar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = BtnDeletarClick
  end
  object Btnatualizar: TButton
    Left = 56
    Top = 152
    Width = 75
    Height = 25
    Caption = 'Atualizar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BtnatualizarClick
  end
  object BtnGravar: TButton
    Left = 56
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Gravar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
  end
  object BtnSalvar: TButton
    Left = 56
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Salvar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
  end
  object EdNome: TEdit
    Left = 144
    Top = 304
    Width = 225
    Height = 27
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
  end
  object EdLocal: TEdit
    Left = 144
    Top = 352
    Width = 225
    Height = 27
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
  end
  object EdValor: TEdit
    Left = 144
    Top = 400
    Width = 225
    Height = 27
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
  end
  object EdAtualizar: TEdit
    Left = 144
    Top = 448
    Width = 225
    Height = 27
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Century Schoolbook'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
  end
end
